﻿using UnityEngine;
using System.Collections;
using System;

public enum WeaponState { Reloading, ReadyToFire }

public class Weapon : Photon.MonoBehaviour {

    [SerializeField] private float timeBetweenShots;
    [SerializeField] private int maxAmmo;
    [SerializeField] private int damage;

    public AudioClip shotSound;

    private Animator animator;
    private WeaponState currentState;
    private int currentAmmo;
    private float shootDelay;

    public string Ammo
    {
        get
        {
            if (currentState == WeaponState.ReadyToFire)
                return currentAmmo + "/" + maxAmmo;
            else return "Reloading";
        }
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        currentAmmo = maxAmmo;
        currentState = WeaponState.ReadyToFire;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && currentState != WeaponState.Reloading)
            StartReload();

        if (currentAmmo <= 0 && currentState != WeaponState.Reloading)
            StartReload();
    }
    
    void StartReload()
    {
        animator.SetTrigger("Reload");
        StartCoroutine(Reload());
    }   
     
    public void Shoot(PlayerManager player)
    {
        RaycastHit hit;

        if(currentState == WeaponState.ReadyToFire && Time.time > shootDelay)
        {
            //Trigger shoot animation and play audio
            animator.SetTrigger("Shoot");
            player.PlayAudio();

            //
            shootDelay = Time.time + timeBetweenShots;
            currentAmmo--;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100))
            {
                if (hit.transform.CompareTag("Player"))
                {
                    //Display hit marker
                    player.ui.DisplayHit();

                    HitPlayer(hit.transform);

                    //Check if the player dies by this shot if it does we can increment our kill score.
                    PlayerManager otherPlayer = hit.transform.GetComponent<PlayerManager>();
                    if (otherPlayer.health - damage <= 0)
                        player.kills++;
                }
            }
        }
    }

    void HitPlayer(Transform other)
    {
        PlayerManager otherPlayer = other.GetComponent<PlayerManager>();
        otherPlayer.TakeDamage(PhotonNetwork.player.ID, damage);
    }

    //Reload current Weapon
    IEnumerator Reload()
    {
        currentState = WeaponState.Reloading;
        yield return new WaitForSeconds(2);
        currentAmmo = maxAmmo;
        currentState = WeaponState.ReadyToFire;
    }

    //Placeholder method for resetting ammo when respawning.
    public void Reset()
    {
        currentAmmo = maxAmmo;
    }
}
