﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerUI : MonoBehaviour {

    [SerializeField] private Text playerNames;
    [SerializeField] private Text playerKills;
    [SerializeField] private Text playerDeaths;
    [SerializeField] private Image hitDisplay;

    public GameObject scoreBoard;

    private PlayerManager player;
    private GameObject UI;

    public Slider healthBar;
    public Text ammoText;
    private bool didUpdate = false;
	
	// Update is called once per frame
	void Update () {
        if (player)
        {
            healthBar.value = player.health;
            ammoText.text = player.currentWeapon.Ammo;

            if (Input.GetKey(KeyCode.Tab))
            {
                if (didUpdate == false)
                    UpdateScoreBoard();
                DisplayScoreBoard(true);
            }
            else DisplayScoreBoard(false);
        }
	}

    public void ResetValues()
    {
        healthBar.maxValue = player.maxHealth;
        healthBar.value = player.health;
    }

    public void AssignPlayer(PlayerManager player)
    {
        this.player = player;
        hitDisplay.canvasRenderer.SetAlpha(0);
        ResetValues();
    }

    public void UpdateScoreBoard()
    {
        didUpdate = true;
        var players = FindObjectsOfType<PlayerManager>();

        playerNames.text = "";
        playerKills.text = "";
        playerDeaths.text = "";

        foreach(var player in players)
        {
            playerNames.text += player.PlayerName + "\n";
            playerKills.text += player.kills + "\n";
            playerDeaths.text += player.deaths + "\n";
        }
    }

    void DisplayScoreBoard(bool shouldActivate)
    {
        scoreBoard.SetActive(shouldActivate);
        if (shouldActivate == false)
            didUpdate = false;
    }

    public void DisplayHit()
    {
        hitDisplay.canvasRenderer.SetAlpha(1);
        hitDisplay.CrossFadeAlpha(0, .5f, false);
    }
}
