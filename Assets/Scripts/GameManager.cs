﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : Photon.MonoBehaviour {

    [SerializeField] private GameObject playerPrefab;
    public Transform[] spawnPoints;

    public GameObject worldCamera;

	void Start ()
    {
        //If we arent connected with photon we should return to the launcher scene.
        if (PhotonNetwork.connected == false)
        {
            SceneManager.LoadScene("Launcher");
            return;
        }

        //Increase sendrate
        PhotonNetwork.sendRate = 30;
        PhotonNetwork.sendRateOnSerialize = 20;

        //Spawn the player when he joins the room.
        spawnPoints = GameObject.Find("SpawnPoints").GetComponentsInChildren<Transform>();
        SpawnPlayer();
    }

    private void OnPhotonRandomJoinFailed()
    {
        PhotonNetwork.CreateRoom("Shooting and stuff");
    }

    void SpawnPlayer()
    {
        if(worldCamera) worldCamera.SetActive(false);

        GameObject player = PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity, 0);
        if (player)
        {
            PlayerManager pm = player.GetComponent<PlayerManager>();
            pm.ActivatePlayer();
        }
    }

    //Method for leave room button
    public void LeaveRoom()
    {        
        PhotonNetwork.LeaveRoom();
    }

    //Gets called when the player leaves the room
    void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        JoinedGameMessage(false, other.name);
    }

    //Gets called when the player enters the room
    void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        JoinedGameMessage(true, other.name);
    }

    void OnLeftRoom()
    {
        SceneManager.LoadScene("Launcher");
    }

    public Transform GetSpawnPoint()
    {
        if (spawnPoints == null || spawnPoints.Length == 0)
            Debug.LogError("No SpawnPoints???");
        return spawnPoints[Random.Range(0, spawnPoints.Length)];
    }

    /// <summary>
    /// Gets playerName by photon ID
    /// </summary>
    /// <param name="playerID">The photon id of the player</param>
    /// <returns>The playerName with the supplied id</returns>
    public string GetPlayerName(int playerID)
    {
        string playerName = "";
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            if (player.ID == playerID)
            {
                playerName = player.name;
            }
        }
        return playerName;
    }
    
    //Displays message on screen if someone leaves/joins the room.
    void JoinedGameMessage(bool didJoin, string playerName)
    {
        if (didJoin)
            AlertManager.Alert(playerName + " has joined the game", Color.white);
        else
            AlertManager.Alert(playerName + " has left the game", Color.white);
    }        
}
