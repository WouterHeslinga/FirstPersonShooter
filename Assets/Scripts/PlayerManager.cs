﻿using UnityEngine;
using System.Collections;

public enum PlayerState { Alive, Dead, Spawning }

public class PlayerManager : Photon.MonoBehaviour {

    [SerializeField] private PlayerController playerController;
    [SerializeField] private GameObject playerCamera;

    public PlayerUI ui;
    public Weapon currentWeapon;
    public PlayerState currentState;
    public float health;
    public float maxHealth = 100;
    private GameManager gameManager;
    private AudioClip shotSound; //Audio clip for the equipped weapon

    //Stats
    public int kills;
    public int deaths;

    //Returns the player name of the current photon view.
    public string PlayerName
    {
        get
        {
            if (photonView && photonView.owner != null)
            {
                return photonView.owner.name;
            }
            else return "Player";
        }
    }

    //Returns the id of the current photon view.
    public int PhotonID { get { return photonView.ownerId; } }

    void Awake()
    {
        if (!photonView.isMine)
        {
            playerController.enabled = false;
            playerCamera.SetActive(false);
        }
        gameManager = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (!photonView.isMine)
            return;

        if (currentState != PlayerState.Alive)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    public void ActivatePlayer()
    {
        if (!photonView.isMine)
            return;

        transform.name = PlayerName;
        Respawn();

        //Get an UI if we dont have one.
        if (!ui)
        {
            ui = GameObject.Find("UI").GetComponent<PlayerUI>();
            ui.AssignPlayer(this);
        }
    }

    //Calls the takedamage RPC
    public void TakeDamage(int shooterID, float damage)
    {
        if (photonView)
            photonView.RPC("RPCTakeDamage", PhotonTargets.All, shooterID, damage);
    }

    //Calls the playAudio RPC
    public void PlayAudio()
    {
        if (photonView)
            photonView.RPC("RPCPlayAudio", PhotonTargets.All);
    }

    //Gets called when the player dies
    void OnDead(int shooterID)
    {
        if (currentState == PlayerState.Dead)
            return;

        //We died increment deaths
        deaths++;
        currentState = PlayerState.Dead;
        transform.position = new Vector3(999, 999, 999); //TODO: better respawn
        StartCoroutine(WaitToRespawn());

        string killerName = gameManager.GetPlayerName(shooterID);        

        //Calls the DeathAlert RPC so players can see if someone gets killed.
        if (photonView)
            photonView.RPC("DeathAlert", PhotonTargets.All, PlayerName, killerName);
    }

    //Respawns the player in a set amount of time and shows a countdown on screen.
    IEnumerator WaitToRespawn()
    {
        for (int i = 5; i > 0; i--)
        {
            AlertManager.Alert(i.ToString(), Color.red);
            yield return new WaitForSeconds(1);
        }
        Respawn();
    }

    //Respawns the player at a spawnpoint and sets hp back to full.
    void Respawn()
    {
        health = maxHealth;
        currentState = PlayerState.Alive;

        //Set position
        Transform spawnPoint = gameManager.GetSpawnPoint();
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;

        //Placeholder to reset weapon ammo
        currentWeapon.Reset();
    }

    //Shoots the current weapon
    void Shoot()
    {
        if (currentWeapon)
            currentWeapon.Shoot(this);
    }

    //Gets called when the current player takes damage.
    [PunRPC]
    void RPCTakeDamage(int shooterID, float amount)
    {
        if (photonView.isMine)
        {
            if (currentState == PlayerState.Dead)
                return;
            health = Mathf.Clamp(health - amount, 0, maxHealth);

            if (health <= 0)
            {
                OnDead(shooterID);
            }
        }
    }

    //Gets called when a player dies, shows on screen who killed who
    [PunRPC]
    public void DeathAlert(string victimName, string killerName)
    {
        AlertManager.Alert(killerName + " killed " + victimName, Color.white);
    }

    //Current player instantiates audio in the scene.
    //FIXME: Doesn't work over network properly???
    [PunRPC]
    void RPCPlayAudio()
    {
        if(photonView.isMine)
            AudioSource.PlayClipAtPoint(currentWeapon.shotSound, transform.position);
    }

    //Serializes data with the server and other players.
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(health);
            stream.SendNext((byte)currentState);
            stream.SendNext(kills);
            stream.SendNext(deaths);
        }
        else
        {
            health = (float)stream.ReceiveNext();
            currentState = (PlayerState)(byte)stream.ReceiveNext();
            kills = (int)stream.ReceiveNext();
            deaths = (int)stream.ReceiveNext();
        }
    }
}
